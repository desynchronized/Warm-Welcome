const Discord = require("discord.js");
const winston = require("winston");

function randomQuote() {
    var file = require("fs").readFileSync("../data/quotes.txt", "utf-8");
    return file.split("\n")[Math.floor(Math.random() * file.split("\n").length)]
}

function determineDefaultChannel(guild) {
    if (guild.channels.find("name", "welcome")) return guild.channels.find("name", "welcome")
    if (guild.channels.find("name", "general")) return guild.channels.find("name", "general")
    return guild.channels.first()
}

module.exports = async function (memberToWelcome) {
    if (!memberToWelcome.guild.settings.get("active", true)) return winston.info("Not welcoming " + memberToWelcome.id + " because welcome is off" ) ? null:null
    var channel = memberToWelcome.guild.settings.get("channel", null)
    if (channel) channel = memberToWelcome.guild.channels.get(channel)
    if (!channel) channel = determineDefaultChannel(memberToWelcome.guild)
    var msg = await channel.send(new Discord.RichEmbed().setTitle("Incoming signal recieved...").setDescription("Welcome <@" + memberToWelcome.id + ">\nPick an Echelon in <#526901640960147456>. Enjoy your stay!").setColor(0xf44274).setImage("https://cdn.discordapp.com/attachments/533117545943662633/540373286635307022/ezgif-1-0dea3f5a1063.gif"))
}
