Sometimes, people only seem determined upon one course because they have been offered no other options.
Let them see my weakness, and let them see me overcome it.
Out-of-the-box is where I live.
If you had ever experienced love, you wouldn't have to ask.
That is sin. That is evil. And you are evil.
The possibility of me being insane has been interfering with my ability to relax.
Perhaps man wasn't meant for paradise. Maybe he was meant to claw, to scratch all the way.
I can't be attached to anything... otherwise I couldn't be a gambler.
I will destroy everything... I will create a monument to non-existence!
Sometimes, even a futile gesture can be worth something.
Rulers change, borders change, countries change, but money is a constant. In fact, nothing better for business than a good war.
Only those close to the top are rewarded without trying. It's the way of the world.
Your actions have meaning only if they hold true to your ideals.
The extent of truth shall never change.
The problem with being clever, is that everyone assumes you are planning something.
You optimists just can't understand that a depressed person doesn't want you to try and cheer them up. It makes us sick.
If you want to tell people the truth, make them laugh, otherwise they’ll kill you.
We are all in the gutter, but some of us are looking at the stars.
It is such a quiet thing, to fall. But far more terrible is to admit it.
Apathy is death.
First of all, your beauty has captivated me! Second... I'm dying to know if I'm your type... I guess your ...flaws... would be a distant third.
Watch your mouth! There are ladies present! And I am a perfect gentleman!
It's okay not to feel whole. Even if you feel only partly complete, if you repeat that enough, it'll eventually be whole. A part... is better than zero.
Childish ideals pale when placed before reality
Music is a mysterious thing. Sometimes it makes people remember things they do not expect. Many thoughts, feelings, memories... things almost forgotten... Regardless of whether the listener desires to remember or not